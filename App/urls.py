"""
URL configuration file for the application.

This file defines the main URLs for the application, including
views for the home page, contact form, authentication,
blog, portfolio, and tutorials.

"""

from django.contrib import admin
from django.urls import path

from core import views as c

from authentication import views as auth
from portfolio import views as portfolio

from blog.views import ArticleListView, ArticleByThemeDetailView
from tutorial.views import TutorialListView, TutorialDetailView


urlpatterns = [
    path("personnal_url_for_admin_dashboard/", admin.site.urls),
    path("", c.index, name="home"),
    path("contactez-moi", c.contact, name="contact"),
    # authentication
    path("login", auth.log_in, name="connexion"),
    path("logout", auth.log_out, name="deconnexion"),
    path("profile", auth.profile, name="profile"),
    path("profile/update", auth.update, name="profile_update"),
    # blog
    path("oodysee-dev", ArticleListView.as_view(), name="articles"),
    path("oodysee-dev/<slug:slug>", ArticleByThemeDetailView.as_view(), name="theme"),
    # portfolio
    path("portfolio", portfolio.achievement, name="portfolio"),
    # tutorial
    path("astuces", TutorialListView.as_view(), name="tutos"),
    path("astuces/<slug:slug>", TutorialDetailView.as_view(), name="tuto-theme"),
    # path("", , name=""),
]
