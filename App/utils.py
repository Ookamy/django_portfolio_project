"""
This module defines classes for handling email functionalities.

The `Mail` class facilitates sending different types of emails such as contact 
messages, newsletters, etc.
It utilizes Django's email functionalities and templates for composing and 
sending emails.

The `HelpMail` class is designed for sending warning emails. It provides a 
simple interface to compose and send warnings.
"""

from django.core.mail import send_mail
from django.template.loader import render_to_string


TEMPLATE = {
    "basic": "mailbox/template/basic.html",
    "contact": "mailbox/template/template_contact.html",
    "newsletter": "mailbox/template/template_newsletter.html",
    "welcome": "mailbox/template/template_welcome.html",
}


class Mail:
    """
    A class to handle email functionalities.

    This class facilitates sending different types of emails such as contact
    messages, newsletters, etc.
    It utilizes Django's email functionalities and templates for composing and
    sending emails.

    Attributes:
        objet (str): The subject of the email.
        from_name (str): The sender's name.
        message (str): The body of the email.
        mail (str): The recipient's email address.
        from_ip (str): The sender's IP address.
        send_de (str): The sender's email address.
        pour (list): A list of recipient email addresses.
        template (str): The template to use for the email.
        title_mail (str): The title of the email.
    """

    def __init__(
        self,
        objet=None,
        from_name=None,
        message=None,
        mail=None,
        from_ip=None,
        send_de=None,
        pour=None,
        template=None,
        title_mail=None,
    ):
        """
        Initializes a Mail object with the provided attributes.

        Args:
            objet (str): The subject of the email.
            from_name (str): The sender's name.
            message (str): The body of the email.
            mail (str): The recipient's email address.
            from_ip (str): The sender's IP address.
            send_de (str): The sender's email address.
            pour (list): A list of recipient email addresses.
            template (str): The template to use for the email.
            title_mail (str): The title of the email.
        """
        self.objet = objet
        self.from_name = from_name
        self.message = message
        self.mail = mail
        self.from_ip = from_ip
        self.send_de = send_de
        self.pour = pour
        self.template = template
        self.title_mail = title_mail

    def send_contact_mail(self):
        """
        Sends a contact email using the specified template.

        The email is composed using the provided attributes and the specified template.
        """
        # TEMPLATE[self.template]
        html_template = render_to_string(
            TEMPLATE[self.template],
            {
                "name": self.from_name,
                "object": self.objet,
                "message": self.message,
                "mail": self.mail,
                "ip": self.from_ip,
                "title_mail": self.title_mail,
            },
        )

        # reconstruire l'objet du mail
        message_objet = (
            f"{self.objet} de {self.from_name} -- {self.mail} -- {self.from_ip}"
        )
        # reconstruire le corps du mail
        message_body = f"""Mail de {self.from_name}:
        {self.message}
        """

        send_mail(
            subject=message_objet,
            message=message_body,
            from_email=self.send_de,
            recipient_list=self.pour,
            html_message=html_template,
            fail_silently=False,
        )

    def send_confirme_contact_mail(self):
        """
        Sends a confirmation email for received contact messages.

        Notifies the sender that their message has been received.
        """
        # reconstruire l'objet du mail
        message_objet = (
            f"Message bien reçus : {self.objet} de {self.from_name} -- {self.mail} "
        )
        # reconstruire le corps du mail
        message_body = f"""Bonjour {self.from_name},
                        Nous avons bien reçus votre message en provenance de notre formulaire de contact.
                        Nous vous repondrons dans les plus bref delais!
                        """
        send_mail(
            subject=message_objet,
            message=message_body,
            from_email=self.send_de,
            recipient_list=self.mail,
            fail_silently=False,
        )


class HelpMail:
    """
    A class for sending warning emails.

    This class provides a simple interface to compose and send warning emails.

    Attributes:
        subject (str): The subject of the warning email.
        message (str): The body of the warning email.
    """

    def __init__(self, subject, message):
        """
        Initializes a HelpMail object with the provided subject and message.

        Args:
            subject (str): The subject of the warning email.
            message (str): The body of the warning email.
        """
        self.subject = subject
        self.message = message

    def send_warning_mail(self):
        """
        Sends a warning email to the specified recipient.

        Uses the provided subject and message to compose and send the warning email.
        """
        send_mail(
            subject=self.subject,
            message=self.message,
            from_email="contact@ookamy.fr",
            recipient_list=["mme.ookamy@gmail.com"],
            fail_silently=False,
        )
