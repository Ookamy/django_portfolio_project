# django_portfolio_project 

Public version of the ookamy-project website (blog/tutorial/portfolio).

## .gitlab-ci.yml

The CI/CD script is not intended for this repository, just to be consulted and show what's up and running on the ookamy.fr site's private repository.


## Getting started

### Setting up the virtual development environment:
1. Generating the virtual environment
    ```
        python -m venv env
    ```
1. Activate virtual environment
    ```
    # unix
        source env/bin/activate
    # result in console  
        [ookamy@ookamy-manjaro django_portfolio_project]$  source env/bin/activate
        (env) [ookamy@ookamy-manjaro django_portfolio_project]$ 
    ```
1. Installing packages
    ```
    # update/upgrad pip
        pip install --upgrade pip
    # use the requirements file
        pip install -r requirements.txt
    ```
    ```
    # install packages directly
        pip install django
        pip install python-dotenv
        pip install cryptography
        pip install django-ckeditor (version django-ckeditor==6.7.0 )
        pip install django-resized
        pip install django-colorfield
        pip install pillow
        pip install whitenoise
    ```
    ```
    # generate the requirements.txt
        pip freeze > requirements.txt
    ```

### /!\ Create dotenv file.
1. dotenv file to be created in / folder
1. .env template
    ``` 
    .env/
        # Environment variable used in the project
        DEV_ENVIRONMENT = "DEV"  # "PROD"
        # Variable for encrypting 'sensitive' user data
        PWD_KEY = "password"
        SALT_KEY = "binarykey"
        IT_KEY = 100000

        # SECURE KEY
        DEV_KEY = 'django-insecure-t_ejq1iqbc6cpuàpjxrs!ceo5n'  # Fake Key
        PROD_KEY = 'django-secure-t_ekh-z1(^fyv#=wvb4yer+jx^)'  # Fake Key

        # DATABASE PRODUCTION
        DB_NAME = 'name_datatable'
        DB_USER = 'username_datatable'
        DB_PASSWORD ='password_datatable'
        DB_HOST = '127.0.0.1'  # mariadb
        DB_PORT = '3306' # mariadb

        # SMTP PRODUCTION
        EMAIL_HOST = ''
        EMAIL_HOST_USER = ''
        EMAIL_HOST_PASSWORD = ''
        EMAIL_PORT = 000
    ```

### commande django:
1. Database installation, migration and dump loading
    ```
        # migrate application
        (env) [django_portfolio_project]$ python manage.py migrate

        # dump loading
        (env) [django_portfolio_project]$ python manage.py loaddata dump.json 
            Installed 100 object(s) from 1 fixture(s)
    ```
1. Start the django application
    ```
    (env) [django_portfolio_project]$ python manage.py runserver
        Watching for file changes with StatReloader
        Performing system checks...

        System check identified no issues (0 silenced).

        January 26, 2024 - 09:07:01
        Django version 5.0.1, using settings 'App.settings'
        Starting development server at http://127.0.0.1:8000/
        Quit the server with CONTROL-C.
    ```


## Superuser 
create super user for superpower

    ```
        python manage.py createsuperuser
        Username (leave blank to use 'ookamy'): yourName
        Email address: yourMail
        Password: yourSuperBadassPassword
        Password (again): yourSuperBadassPassword
        Superuser created successfully.
    ```
