
"""
Django admin configuration for the Profile model.

This module provides the Django admin configuration for the Profile model.
It registers the Profile model with the admin site and customizes the display
of the Profile objects in the admin interface.

Models Registered:
- Profile: It represents the user profile information.

"""
from django.contrib import admin
from authentication.models import Profile


# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """
    Admin configuration for the Profile model.

    Attributes:
    - list_display (tuple): Specifies the fields to display for each Profile object in the admin interface.

    """
    list_display = ("id", "user", "image")
