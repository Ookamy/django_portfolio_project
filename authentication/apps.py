"""
Django application configuration for the authentication app.

This module provides the Django application configuration for the authentication app.
It specifies the default auto field and the name of the app.

"""
from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    """
    Application configuration for the authentication app.

    Attributes:
    - default_auto_field (str): The default auto field used for models.
    - name (str): The name of the app.

    Example:
        To reference this app configuration in Django settings, use:
        AUTHENTICATION_APPS = ['authentication.apps.AuthenticationConfig']
    """
    default_auto_field = "django.db.models.BigAutoField"
    name = "authentication"
