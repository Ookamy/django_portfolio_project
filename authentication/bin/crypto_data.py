"""
CryptoData module for handling encryption and decryption of sensitive data.

Crypto rules:
- User account information, such as passwords, usernames, and e-mail addresses.
- Location information, such as addresses and geolocation information.
"""

import os
import base64
from dotenv import load_dotenv
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

load_dotenv()


class CryptoData:
    """
    Handles encryption and decryption of sensitive data.

    Crypto rules:
    - User account information, such as passwords, usernames, and e-mail addresses.
    - Location information, such as addresses and geolocation information.

    Attributes:
    - password (bytes): The password used for key derivation.
    - salt (bytes): The salt used for key derivation.
    - it (int): The number of iterations for key derivation.
    - kdf (PBKDF2HMAC): Key derivation function using PBKDF2 with SHA256.
    - encrypt_key (bytes): Encrypted key derived from the password.
    - f (Fernet): Fernet cipher instance for encryption and decryption.
    """

    def __init__(self):
        """
        Initialize the CryptoData class with environment variables for encryption and decryption.
        """
        self.password = os.getenv("PWD_KEY").encode("utf-8")
        self.salt = os.getenv("SALT_KEY").encode("utf-8")
        self.it = int(os.getenv("IT_KEY"))
        self.kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=self.salt,
            iterations=self.it,
            backend=default_backend(),
        )
        self.encrypt_key = base64.urlsafe_b64encode(self.kdf.derive(self.password))

        self.f = Fernet(self.encrypt_key)

    def encrypt(self, data_to_encrypt: str = None):
        """
        Encrypt the provided data using Fernet encryption.

        Args:
        - data_to_encrypt (str): The data to be encrypted.

        Returns:
        - str: The encrypted data token.

        Raises:
        - AttributeError: If the data to encrypt is not provided.
        - Exception: For any other unexpected errors.
        """
        if data_to_encrypt is None:
            raise ValueError("The data to encrypt is not provided.")

        if not isinstance(data_to_encrypt, str):
            raise TypeError("Data to encrypt must be a string.")

        try:
            data_to_encrypt = data_to_encrypt.encode("utf-8")
            token = self.f.encrypt(data_to_encrypt)
            return token.decode("utf-8")
        except AttributeError as ae:
            return f"the information to be encrypted is not indicated : {ae}\n{data_to_encrypt}"
        except Exception as e:
            return f"An error has occurred : {e}"

    def decrypt(self, data_to_decrypt: str = None):
        """
        Decrypt the provided data using Fernet decryption.

        Args:
        - data_to_decrypt (str): The encrypted data token.

        Returns:
        - str: The decrypted data.

        Raises:
        - AttributeError: If the data to decrypt is not provided.
        - Exception: For any other unexpected errors.
        """
        if data_to_decrypt is None:
            raise ValueError("The data to decrypt is not provided.")

        if not isinstance(data_to_decrypt, str):
            raise TypeError("Data to decrypt must be a string.")

        try:
            data_to_decryp = data_to_decrypt.encode("utf-8")
            token = self.f.decrypt(data_to_decryp)
            return token.decode("utf-8")
        except AttributeError as ae:
            return f"the information to be encrypted is not indicated : {ae}\n{data_to_decryp}"
        except Exception as e:
            return f"An error has occurred : {e}"


# if __name__ == "__main__":
#     # encrypte = os.getenv("PWD_KEY")
#     # encrypte = os.getenv("DEV_ENVIRONMENT")
#     cryptodata = CryptoData()
#     data_to_encrypte = "i'm here for test CryptoData class"
#     encrypte = cryptodata.encrypt(data_to_encrypte)
#     print(encrypte)
#     decrypte = cryptodata.decrypt(encrypte)
#     print(decrypte)
