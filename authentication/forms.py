"""
Django forms for user registration, profile management, and password change.

This module provides Django forms for:
- User registration
- User profile update
- User password change

Forms:
- RegisterForm: Inherits from UserCreationForm, adds email field.
- UserUpdateForm: Inherits from UserChangeForm.
- ChangePasswordForm: Inherits from PasswordChangeForm.
- ProfileUpdateForm: Form for updating the user profile image.

"""

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    UserCreationForm,
    UserChangeForm,
    PasswordChangeForm,
)
from authentication.models import Profile


class RegisterForm(UserCreationForm):
    """
    Form for user registration.

    Inherits from UserCreationForm and adds an email field.

    Attributes:
    - email (forms.EmailField): Email field for user registration.

    Example:
        To use this form in a Django view:
        form = RegisterForm(request.POST)
    """

    email = forms.EmailField()

    class Meta(UserCreationForm.Meta):
        """
        Meta class for RegisterForm.

        Attributes:
        - model (User): The user model used for registration.
        - fields (list): The fields to be displayed in the form.

        Example:
            model = User
            fields = ["username", "email", "password1", "password2"]
        """

        model = User
        fields = ["username", "email", "password1", "password2"]


class UserUpdateForm(UserChangeForm):
    """
    Form for updating user information.

    Inherits from UserChangeForm.

    Example:
        To use this form in a Django view:
        form = UserUpdateForm(request.POST, instance=request.user)
    """

    class Meta(UserChangeForm.Meta):
        """
        Meta class for UserUpdateForm.

        Attributes:
        - model (User): The user model used for updating user information.
        - fields (list): The fields to be displayed in the form.

        Example:
            model = User
            fields = ["username", "email"]
        """

        model = User
        fields = ["username", "email"]


class ChangePasswordForm(PasswordChangeForm):
    """
    Form for changing user password.

    Inherits from PasswordChangeForm.

    Example:
        To use this form in a Django view:
        form = ChangePasswordForm(request.user, request.POST)
    """

    class Meta(PasswordChangeForm):
        """
        Meta class for ChangePasswordForm.

        Attributes:
        - model (User): The user model used for changing the password.
        - fields (str): All fields are used for this form.

        Example:
            model = User
            fields = "__all__"
        """

        model = User
        fields = "__all__"


class ProfileUpdateForm(forms.ModelForm):
    """
    Form for updating the user profile image.

    Example:
        To use this form in a Django view:
        form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
    """

    class Meta:
        """
        Meta class for ProfileUpdateForm.

        Attributes:
        - model (Profile): The profile model used for updating the user profile image.
        - fields (list): The fields to be displayed in the form.

        Example:
            model = Profile
            fields = ["image"]
        """

        model = Profile
        fields = ["image"]
