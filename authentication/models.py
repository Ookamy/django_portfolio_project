"""
Django models for user authentication and profile management.

This module provides the Django model for:
- User Profile: Extends the built-in User model to include additional profile information.

Models:
- Profile: OneToOne relationship with the User model, includes user profile information and image.

Functions:
- rename: Renames and handles the profile image file, deleting the existing 
file and replacing it with the new one.

"""

import os
from django.db import models
from django.contrib.auth.models import User
from django_resized import ResizedImageField
from App.utils import HelpMail


# Deletes the existing file and replaces it with the new one =)
def rename(instance, nom):
    """
    Rename and handle the profile image file.

    Deletes the existing file and replaces it with the new one.

    Args:
    - instance (Profile): The Profile instance.
    - nom (str): The new filename.

    Returns:
    - str: The new file path for the profile image.

    Example:
        rename(instance, "new_avatar.png")
    """
    profile_instance = Profile.objects.get(pk=instance.pk)
    ext = os.path.splitext(nom)[1]
    if profile_instance.image and profile_instance.image != "default.png":
        img = profile_instance.image
        try:
            if img.file:
                if os.path.isfile(img.path):
                    img.file.close()
                    os.remove(img.path)
        except FileNotFoundError:
            # send an e-mail to look at this and solve the problem
            HelpMail(
                subject="Problème de suppression d'avatar",
                message=f"""problème de suppression : {img}
                User : {profile_instance.user} - {instance.user.username}
                path : {img.path}
                nouveau fichier : {nom}
                nouveau nom : profile_pics/{instance.user.username}{ext}
                """,
            ).send_warning_mail()

    return f"profile_pics/{instance.user.username}{ext}"


# Add user Profil for more information
class Profile(models.Model):
    """
    Model for user profile information.

    Attributes:
    - user (models.OneToOneField): The related User model.
    - image (ResizedImageField): The profile image, resized and cropped to a square format.

    Example:
        To create a new Profile instance:
        profile = Profile.objects.create(user=user_instance, image="new_avatar.png")
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # cuts image to square format
    image = ResizedImageField(
        size=[300, 300],
        crop=["middle", "center"],
        default="default.png",
        upload_to=rename,
    )

    def __str__(self):
        """
        String representation of the Profile instance.

        Returns:
        - str: The username followed by "Profile".
        """
        return f"{self.user.username} Profile"  # show how we want it to be displayed

    def save(self, *args, **kwargs):
        """
        Save method for the Profile model.

        Calls the superclass save method after saving the instance.

        Example:
            To save a Profile instance:
            profile.save()
        """
        # super(Profile, self).save(*args, **kwargs)
        super().save(*args, **kwargs)  # Python 3 style super()
