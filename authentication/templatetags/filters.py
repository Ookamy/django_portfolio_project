"""
Django template filters for handling encryption and decryption of sensitive data.

This module provides a custom Django template filter to decrypt encrypted data 
using the CryptoData class
from the authentication.bin.crypto_data module.

Usage:
- Use the 'decrypte' filter in Django templates to decrypt encrypted data.

Example:
    {{ encrypted_data|decrypte }}

"""

from django import template
from authentication.bin.crypto_data import CryptoData

register = template.Library()


@register.filter(name="decrypte")
def decrypted(data):
    """
    Decrypt the provided encrypted data using the CryptoData class.

    Args:
    - data (str): The encrypted data to be decrypted.

    Returns:
    - str: The decrypted data.

    Note:
    The decryption is performed using the CryptoData class from authentication.
    bin.crypto_data module.
    """
    return CryptoData().decrypt(data)
