"""
Django views for user authentication and profile management.

This module provides Django views for:
- User login
- User logout
- User profile management
- User profile update

Views:
- log_in: View for user login.
- log_out: View for user logout.
- profile: View for displaying and updating the user profile.
- update: View for updating the user profile.

"""

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist

from blog.models import Article
from tutorial.models import Tutorial
from authentication.models import Profile
from authentication.bin.crypto_data import CryptoData
from authentication.forms import UserUpdateForm, ProfileUpdateForm


def log_in(request):
    """
    View for user login.

    Authenticates the user and logs them in.

    Example:
        To use this view in a Django template:
        {% url 'log_in' %}

    """
    if request.user.is_authenticated:
        return redirect("profile")
    if request.method == "POST":
        login_form = AuthenticationForm(request=request, data=request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data.get("username")
            password = login_form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, "Profile connecté.")
                return redirect("profile")
            messages.error(request, "Mot de passe ou Identifiant invalid")
        messages.error(request, "Mot de passe ou Identifiant invalid")

    login_form = AuthenticationForm()
    return render(request, "authentication/connexion.html", {"login_form": login_form})


def log_out(request):
    """
    View for user logout.

    Logs out the user and redirects them to the home page.

    Example:
        To use this view in a Django template:
        {% url 'log_out' %}

    """
    logout(request)
    return redirect("home")


@login_required
def profile(request):
    """
    View for displaying and updating the user profile.

    Displays the user profile and handles the profile update.

    Example:
        To use this view in a Django template:
        {% url 'profile' %}

    """
    if request.method == "POST":
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(
            request.POST, request.FILES, instance=request.user.profile
        )
        if u_form.is_valid() and p_form.is_valid():
            instance = u_form.save(commit=False)
            instance.email = CryptoData().encrypt(instance.email)  # encrypter le mail
            instance.save()

            p_form.save()
            messages.success(request, "Votre compte a été mis à jour !")
            return redirect("profile")  # Redirect back to profile page

    try:
        user_p = request.user.profile
    except ObjectDoesNotExist:
        user_p = Profile(user=request.user)
        user_p.save(force_insert=False)

    # Décrypter l'email avant de l'afficher dans le formulaire
    decrypted_email = CryptoData().decrypt(request.user.email)
    u_form = UserUpdateForm(instance=request.user, initial={"email": decrypted_email})
    p_form = ProfileUpdateForm(instance=request.user.profile)

    return render(
        request,
        "authentication/profile.html",
        {
            "u_form": u_form,
            "p_form": p_form,
            "dic_art_tuto": {
                "articles": Article.objects.filter(author=request.user),
                "tutorials": Tutorial.objects.filter(author=request.user),
            },
        },
    )


@login_required
def update(request):
    """
    View for updating the user profile.

    Handles the user profile update.

    Example:
        To use this view in a Django template:
        {% url 'update' %}

    """
    if request.method == "POST":
        update_form = UserUpdateForm(request.POST, instance=request.user)
        if update_form.is_valid():
            instance = update_form.save(commit=False)
            instance.email = CryptoData().encrypt(instance.email)  # encrypter le mail
            user = instance.save()
            # user = update_form.save()
            login(request, user)
            messages.success(request, "Compte modifier avec succes.")
            return redirect("profil")
        messages.error(request, "Oups! erreur dans la matrice!")
    update_form = UserUpdateForm(instance=request.user)
    return render(request, "authentication/profil_update.html", {"form": update_form})
