from django.contrib import admin
from blog.models import Tag, Theme, Article


# Register your models here.
@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ("id", "title", "describe", "order", "active")


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("id", "tag_name")


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = (
        "id",
        "title",
        "theme",
        "subtitle",
        "image",
        "text",
        "author",
        "date",
        "order",
        "active",
    )
