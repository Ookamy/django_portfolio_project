from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.urls import reverse


class Theme(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    describe = models.CharField(max_length=500, null=True, blank=True)
    order = models.IntegerField(default=0)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return f"{self.title}"

    def get_absolute_url(self):
        return reverse("theme_detail", kwargs={"slug": self.slug})


class Tag(models.Model):
    tag_name = models.CharField(max_length=100)

    class Meta:
        ordering = ["tag_name"]

    def __str__(self):
        return f"{self.tag_name}"


class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE)
    subtitle = models.CharField(max_length=500, null=True, blank=True)
    text = RichTextField(blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="blog", null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True, editable=False)
    order = models.IntegerField(default=0)
    active = models.BooleanField(default=True)
    tags = models.ManyToManyField(Tag, blank=True, related_name="articles")

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return f"{self.title}"

    def get_absolute_url(self):
        return reverse("article_detail", kwargs={"slug": self.slug})
