from django.shortcuts import render

from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required
from django.contrib import messages


from blog.models import Article, Theme


# Tous les articles
class ArticleListView(ListView):
    paginate_by = 4
    model = Article
    template_name = "blog/blogs.html"

    def get_queryset(self):
        return super().get_queryset().filter(active=True).filter(theme__active=True)

    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)
        # context['object_list'] = Article.objects.all().filter(active=True)
        context["object_list"] = self.get_queryset()
        return context


# Le detail d'un seul article
class ArticleDetailView(DetailView):
    model = Article
    template_name = "blog/blog.html"

    def get_queryset(self):
        return super().get_queryset().filter(active=True).filter(theme__active=True)

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context["object_list"] = self.get_queryset()
        return context


# Tous les articles d'un seule thème
class ArticleByThemeDetailView(DetailView):
    model = Theme
    template_name = "blog/blog_theme.html"

    def get_queryset(self):
        return super().get_queryset().filter(active=True)

    def get_context_data(self, **kwargs):
        context = super(ArticleByThemeDetailView, self).get_context_data(**kwargs)
        # context['object_list'] = Article.objects.all().filter(active=True).filter(theme__active=True)
        return context
