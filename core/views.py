from django.shortcuts import render
from django.contrib import messages
from core.forms import ContactForm
from App.utils import Mail


def index(request):
    return render(request, "index.html")


def contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)

        if form.is_valid():
            message = form.save()

            Mail(
                objet="Message",
                from_name=form.cleaned_data["name"],
                message=form.cleaned_data["message"],
                mail=form.cleaned_data["email"],
                send_de="contact@ookamy.fr",
                pour=["mme.ookamy@gmail.com"],
                template="basic",
                title_mail="Formulaire De Contact",
            ).send_contact_mail()

            messages.success(request, "Message Envoyé")
        else:
            messages.error(request, "Erreur pendant l'envoie")

    form = ContactForm()
    return render(request, "contact.html", {"form": form})
