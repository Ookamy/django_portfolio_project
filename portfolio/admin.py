from django.contrib import admin
from portfolio.models import Link, ProgramLanguage, Achievement, Screenshot


# Register your models here.


@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "url")


@admin.register(ProgramLanguage)
class CodeAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


@admin.register(Screenshot)
class ImagePortfolioAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "img")


@admin.register(Achievement)
class AchievementAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "subtitle", "image", "describe")
