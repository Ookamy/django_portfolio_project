from django.db import models
from ckeditor.fields import RichTextField


class Link(models.Model):
    url = models.CharField(max_length=100, unique=True)
    title = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return f"{self.title} - {self.url}"


class ProgramLanguage(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return f"{self.name}"


class Screenshot(models.Model):
    title = models.CharField(max_length=50, unique=True)
    img = models.ImageField(upload_to="portfolio/screenshot", null=True, blank=True)

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return f"{self.title}"


class Achievement(models.Model):
    title = models.CharField(max_length=50)
    subtitle = models.CharField(max_length=100)
    describe = RichTextField(blank=True, default="")
    image = models.ImageField(upload_to="portfolio", null=True, blank=True)
    screen = models.ManyToManyField(Screenshot, blank=True, related_name="achievements")
    code = models.ManyToManyField(
        ProgramLanguage, blank=True, related_name="achievements"
    )
    url = models.ManyToManyField(Link, blank=True, related_name="achievements")
    order = models.IntegerField(default=0)

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return f"{self.title} - {self.code.name} - {self.url} - "
