from django.shortcuts import render

from portfolio.models import Achievement


def achievement(request):
    return render(
        request, "portfolio/portfolio.html", {"achievements": Achievement.objects.all()}
    )
