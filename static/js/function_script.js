/**
 * @returns a random quote that is filled in the table
 * use in setInterval("$('h1').html(randomSentence())", 1000);
 */
function randomSentence() {
    const sentences = [
        "Follow My Dream",
        "Start My Wave",
        "Canada, i'm coming",
        "Just a Drop of Water",
    ];
    // random index
    let index = Math.round(Math.random() * (sentences.length-1));
    return sentences[index]
};

/**
 * For used randomSentence function
 * we can add .toUpperCase() after randomSentence().toUpperCase()
 */
// setInterval("$('h1').html(randomSentence())", 1000);

/**
 * For change text and add letter by letter
 * Used with setInterval("changeMessage()",100);
 */
 let sentence =  randomSentence()
 let lengthSentence = sentence.length;
 let arrayLetter = sentence.split('');
 
 let texte = [];
 let txt = '';
 
 let nb_msg = lengthSentence - 1;
 
 for (i=0; i<lengthSentence; i++){
    texte[i] = txt + arrayLetter[i];
    txt = texte[i];
 };
 
 actual_texte = 0;
 function changeMessage(){
     document.getElementById("change").innerHTML = texte[actual_texte];
     actual_texte++;
     if(actual_texte >= texte.length){
        actual_texte = nb_msg;
     }
 };


 
