
/**
 * Scrip JS Ookamy
 * version 1.0
 */
$( document ).ready(function() {
    // auto-copperfields callout-alerte 
    $('#alert-message').delay(2000).fadeOut();
    $('#callout-kill').delay(3000).fadeOut();
}); 


let typed = new Typed("#random", {
    strings: [
        "Follow My Dream",
        "Start My Wave",
        "Canada, i'm coming",
        "Just a Drop of Water",
        "Je code comme une noob, mais je m'améliore chaque jour",
        "Python, PHP, Javascript ..."
    ],
    typeSpeed: 100,
    backSpeed: 200,
    backDelay: 700,
    loop: true
})


