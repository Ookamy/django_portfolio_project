from django.contrib import admin
from tutorial.models import Tag, Tutorial, Theme


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ("id", "title", "slug", "describe", "active")


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("id", "tag_name")


@admin.register(Tutorial)
class TutorialAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = (
        "id",
        "title",
        "theme",
        "subtitle",
        "author",
        "date_create",
        "date_update",
        "order",
    )
