from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
from django.urls import reverse
from django_resized import ResizedImageField


class Theme(models.Model):
    title = models.CharField(max_length=100)
    describe = models.CharField(max_length=500, null=True, blank=True)
    active = models.BooleanField(default=True)
    slug = models.SlugField(max_length=100)
    img = ResizedImageField(
        size=[300, 300],
        crop=["middle", "center"],
        upload_to="tutorial",
        null=True,
        blank=True,
    )

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return f"{self.title}"


class Tag(models.Model):
    tag_name = models.CharField(max_length=100)

    class Meta:
        ordering = ["tag_name"]

    def __str__(self):
        return f"{self.tag_name}"

    def get_absolute_url(self):
        return reverse("tag", kwargs={"slug": self.slug})


class Tutorial(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    theme = models.ForeignKey(Theme, on_delete=models.SET_NULL, null=True)
    subtitle = models.CharField(max_length=500, null=True, blank=True)
    text = RichTextField(blank=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    order = models.IntegerField(default=0)
    active = models.BooleanField(default=True)
    date_create = models.DateTimeField(auto_now_add=True, editable=False)
    date_update = models.DateTimeField(auto_now=True, editable=True)
    tags = models.ManyToManyField(Tag, blank=True, related_name="tutos")

    class Meta:
        ordering = ["order"]

    def __str__(self):
        return f"{self.theme} : {self.title} "

    def save(self, *args, **kwargs) -> None:
        self.text = mark_safe(self.text)
        super(Tutorial, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("tutorial_detail", kwargs={"slug": self.slug})
