from django.shortcuts import render
from django.views.generic import DetailView, ListView
from tutorial.models import Tutorial, Theme


class TutorialListView(ListView):
    model = Tutorial
    template_name = "tutorial/tutorials.html"
    ordering = ["-date_create"]

    def get_queryset(self):
        return super().get_queryset().filter(theme__active=True)

    def get_context_data(self, **kwargs):
        context = super(TutorialListView, self).get_context_data(**kwargs)
        tutos = Tutorial.objects.all().filter(theme__active=True)
        tuto_list = []
        for tuto in tutos:
            t = {"title": tuto.title, "description": tuto.subtitle, "theme": tuto.theme}
            tuto_list.append(t)
        context["tutorials"] = tuto_list
        return context


class TutorialDetailView(DetailView):
    model = Theme
    template_name = "tutorial/tutorial.html"

    def get_queryset(self):
        return super().get_queryset().filter(active=True)

    def get_context_data(self, **kwargs):
        context = super(TutorialDetailView, self).get_context_data(**kwargs)
        context["object_list"] = Tutorial.objects.all().filter(theme__active=True)
        return context
